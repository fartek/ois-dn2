var emoji = {
  ";)"  : "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\">",
  ":)"  : "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\">",
  "(y)" : "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\">",
  ":*"  : "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\">",
  ":("  : "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\">"
};

var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal, geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev': //dodaj se regex ki pregleda ce so narekovaji
      besede.shift();
      var kanal = besede[0].match(/"\w{1,}"/);;
      console.log(besede);
      if(besede.length == 2)
      {
        //console.log("Ustvarjen kanal z geslom");
        
        var geslo = besede[1].match(/"\w{1,}"/);
        //console.log(kanal[0], geslo[0]);
        if(kanal !== null && typeof kanal !== 'undefined' && geslo !== null && typeof geslo !== 'undefined')
        {
          this.spremeniKanal(kanal[0].substring(1, kanal[0].length-1), geslo[0].substring(1, geslo[0].length-1));
        }else{
          console.log("Napaka z narekovaji--z geslom");
          this.socket.emit('feedbackMsg', {kanal: kanal, text: "Napaka z narekovaji"});
        }
        
      }else{
        //console.log("Ustvarjen kanal brez gesla");
        if(kanal !== null && typeof kanal !== 'undefined')
        {
          this.spremeniKanal(kanal[0].substring(1, kanal[0].length-1), null);
        }else{
          console.log("Napaka z narekovaji--brez gesla");
          sendMessage("Napaka z narekovaji");
        }
      }
      
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};