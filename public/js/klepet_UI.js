var swearCache = [];
var swearWordsInCache = false;
var mojKanal = 'Skedenj';

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }
    $('#poslji-sporocilo').val('');
  } else {
    for(var e in emoji)
   {
     while(sporocilo.indexOf(e) > -1)
     {
        sporocilo = sporocilo.replace(e, emoji[e]);
     }
   }
   
   while(sporocilo.indexOf("<script>") > -1)
   {
     sporocilo = sporocilo.replace("<script>", "&lt;script&gt;");
   }
   while(sporocilo.indexOf("</script>") > -1)
   {
     sporocilo = sporocilo.replace("</script>", "&lt;/script&gt;");
   }
   
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));

    var lines = [];
    var xmlHttp = new XMLHttpRequest();
    var re = /(\w+)/gi;
    var reMatch = sporocilo.match(re);
    var alreadyCached = false;
    
    xmlHttp.onreadystatechange=function()
    {
      if (xmlHttp.readyState==4 && xmlHttp.status==200)
      {
        lines = xmlHttp.responseText.split("\n");
        for(var i=0;i<lines.length;i++)
        {
          if(reMatch !== null)
          {
            for(var j=0; j<reMatch.length;j++)
            {
             // console.log("reMatch:", reMatch[j], "line:", lines[i]);
              if(reMatch[j] === lines[i])
              {
                //console.log("MATCH!");
                var blockedMsg = "";
                for(var c=0;c<reMatch[j].length; c++) // heh, c++ :)
                {
                   blockedMsg = blockedMsg + "*";
                }
                sporocilo = sporocilo.replace(new RegExp("\\b"+reMatch[j]+"\\b","gi"), blockedMsg);
                break;
              }
            }
          }
          
          alreadyCached = false;
          for(var k=0;k<swearCache.length;k++)
          {
            if(swearCache[k] === lines[i])
            {
              alreadyCached = true;
              break;
            }
          }
          if(!alreadyCached)
          {
            swearCache.push(lines[i]);
          }
        }
        
        //console.log("sending msg from read file");
        klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
        $('#sporocila').append(divElementEnostavniTekst(sporocilo));
        $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
        $('#poslji-sporocilo').val('');
      }
    }
    
    
    if(swearWordsInCache && sporocilo.length > 0)
    {
      for(var i=0;i<swearCache.length;i++)
      {
        if(reMatch !== null)
        {
          for(var j=0; j<reMatch.length;j++)
          {
            if(reMatch[j] === swearCache[i])
            {
              var blockedMsg = "";
              for(var c=0;c<reMatch[j].length; c++) // heh, c++ :)
              {
                blockedMsg = blockedMsg + "*";
              }
              sporocilo = sporocilo.replace(new RegExp("\\b"+reMatch[j]+"\\b","gi"), blockedMsg);
              break;
            }
          }
        }
      }
      //console.log("sending msg from cache");
      klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
      $('#poslji-sporocilo').val('');
    }else if(sporocilo.length > 0){
      swearWordsInCache = true;
      xmlHttp.open("GET","/swearWords.txt", true);
      xmlHttp.send();
    }
  }
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.nickname + " @ " + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    mojKanal = rezultat.kanal;
  });
  
  function sendMessage(txt) {
    $('#sporocila').append(divElementHtmlTekst(txt));
  };

  socket.on('sporocilo', function (sporocilo) {
    
   while(sporocilo.besedilo.indexOf("<script>") > -1)
   {
     sporocilo.besedilo = sporocilo.besedilo.replace("<script>", "&lt;script&gt;");
   }
   while(sporocilo.besedilo.indexOf("</script>") > -1)
   {
     sporocilo.besedilo = sporocilo.besedilo.replace("</script>", "&lt;/script&gt;");
   }
   
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('updateUserList', function(kanali){
    $('#seznam-uporabnikov').text('');
    //console.log("prejel userlist:", kanali.userlist);
    for(var k in kanali.userlist)
    {
      if(kanali.userlist[k] !== '' && kanali.userlist[k] !== null && kanali.kanal == mojKanal)///dodaj ka preveri ce je mojkanal == k
      {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(kanali.userlist[k]));
      }
    }
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});