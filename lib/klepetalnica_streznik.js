var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};

var nickname;

var sockets = [];
var uporabnikiNaKanalih = {};

//Opravicujem se vnaprej za ogabno kodo :( 
//It works though...kind of...maybe...

var gesla = {};

//comment za zacetni commit


exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    cakajNaPridruzitevKanalu(socket);
    pridruzitevKanalu(socket, 'Skedenj', null);
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    
    (function(){
      var duplicate = false;
      for(var i=0;i<sockets.length;i++)
      {
        if(socket === sockets[i])
        {
          duplicate = true;
          break;
        }
      }
      if(!duplicate)
      {
        sockets.push(socket);
      }
    }());
    
    
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
      for(var g in gesla)
      {
        var kanalObstaja = false;
        for(var r in io.sockets.manager.rooms)
        {
          if(r !== '' && g===r.substring(1,r.length))
          {
            kanalObstaja = true;
          }
        }
        if(!kanalObstaja)
        {
          delete gesla[g];
        }
      }
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  nickname = vzdevek;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  
  return stGosta + 1;
}


function pridruzitevKanalu(socket, kanal, geslo) {
  if(geslo === null)
  {
    var kanalZaklenjen = false;
    for(var g in gesla)
    {
      if(kanal === g)
      {
        kanalZaklenjen = true;
      }
    }
    if(kanalZaklenjen)
    {
      vstopiVKanal(socket,'Skedenj');
    }else{
      vstopiVKanal(socket,kanal);
    }
  }else{
    
    var zeObstaja = false;
    for(var g in gesla)
    {
      if(g === kanal)
      {
        zeObstaja = true;
        break;
      }
    }
    if(zeObstaja)
    {
      if(gesla[kanal] === geslo)
      {

        vstopiVKanal(socket,kanal);
      }else{

        vstopiVKanal(socket,'Skedenj');
      }
    }else{

      gesla[kanal] = geslo;
      vstopiVKanal(socket,kanal);
    }
  }
  
  posodobiUporabnikeNaKanalih(kanal, socket);
  
}

function posodobiUporabnikeNaKanalih(kanal, socket)
{
  var kanalKljuc = null;
  for(var tk in trenutniKanal)
  {
    kanalKljuc = null;
    for(var k in uporabnikiNaKanalih)
    {
      if(k === trenutniKanal[tk])
      {
        kanalKljuc = k;
        break;
      }
    }
    if(kanalKljuc !== null)
    {
      var uporabnikZeNaKanalu = false;
      for(var i=0;i<uporabnikiNaKanalih[kanalKljuc].length; i++)
      {
        if(uporabnikiNaKanalih[kanalKljuc][i] === vzdevkiGledeNaSocket[tk])
        {
          uporabnikZeNaKanalu = true;
          break;
        }
      }
      if(!uporabnikZeNaKanalu)
      {
        uporabnikiNaKanalih[kanalKljuc].push(vzdevkiGledeNaSocket[tk]); 
      }
    }else{
      uporabnikiNaKanalih[trenutniKanal[tk]] = [vzdevkiGledeNaSocket[tk]];
    }
  }
 
  var socketDuplicate = false;
  for(var i=0;i<sockets.length;i++)
  {
    if(sockets[i] === socket)
    {
      socketDuplicate = true;
      break;
    }
  }
  if(!socketDuplicate)
  {
    sockets.push(socket);
  }
 
  for(var i=0;i<sockets.length;i++)
  {
    if(sockets[i] !== null && typeof sockets[i] !== 'undefined')
    {
      //console.log("osebi",vzdevkiGledeNaSocket[sockets[i].id], trenutniKanal[sockets[i].id], "posiljam",
     // uporabnikiNaKanalih[trenutniKanal[sockets[i].id]]); //need to test
      sockets[i].emit('updateUserList', {
        userlist : uporabnikiNaKanalih[trenutniKanal[sockets[i].id]],
        kanal: trenutniKanal[sockets[i].id]
      });
    }
  }
}

function cakajNaPridruzitevKanalu(socket)
{
  socket.on('posodobiUporabnike', function(sporocilo){
    //console.log("pridruzitev kanalu event fired");
    //posodobiUporabnikeNaKanalih(msg.channel, socket.id);
  });
}

function vstopiVKanal(socket, kanal)
{
  socket.join(kanal);
    trenutniKanal[socket.id] = kanal;
    //socket.emit('pridruzitevOdgovor', {kanal: kanal});
    socket.broadcast.to(kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
    });
    var uporabnikiNaKanalu = io.sockets.clients(kanal);
    if (uporabnikiNaKanalu.length > 1) {
      var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (uporabnikSocketId != socket.id) {
          if (i > 0) {
            uporabnikiNaKanaluPovzetek += ', ';
          }
          uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        }
      }
      uporabnikiNaKanaluPovzetek += '.';
      socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});

    }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        
        for(var i=0;i<uporabnikiNaKanalih[trenutniKanal[socket.id]].length;i++)
        {
          if(uporabnikiNaKanalih[trenutniKanal[socket.id]][i] === vzdevkiGledeNaSocket[socket.id])
          {
            delete uporabnikiNaKanalih[trenutniKanal[socket.id]][i];
            break;
          }
        }
        
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
        posodobiUporabnikeNaKanalih(trenutniKanal[socket.id], socket);
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    if(uporabnikiNaKanalih[trenutniKanal[socket.id]] !== null && typeof uporabnikiNaKanalih[trenutniKanal[socket.id]] !== 'undefined')
    {
      for(var i=0;i<uporabnikiNaKanalih[trenutniKanal[socket.id]].length;i++)
      {
        if(uporabnikiNaKanalih[trenutniKanal[socket.id]][i] === vzdevkiGledeNaSocket[socket.id])
        {
          delete uporabnikiNaKanalih[trenutniKanal[socket.id]][i];
          break;
        }
      }
    }
    pridruzitevKanalu(socket, kanal.novKanal, kanal.geslo);

  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('disconnect', function() {
    //console.log(vzdevkiGledeNaSocket[socket.id], "se poslavlja!");
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    
    //console.log(uporabnikiNaKanalih[trenutniKanal[socket.id]], vzdevkiGledeNaSocket[socket.id]);
    if(uporabnikiNaKanalih[trenutniKanal[socket.id]] !== null  && typeof uporabnikiNaKanalih[trenutniKanal[socket.id]] !== 'undefined')
    {
      for(var i=0;i<uporabnikiNaKanalih[trenutniKanal[socket.id]].length;i++)
      {
        if(uporabnikiNaKanalih[trenutniKanal[socket.id]][i] === vzdevkiGledeNaSocket[socket.id])
        {
          //console.log("brisem", vzdevkiGledeNaSocket[socket.id], "iz", trenutniKanal[socket.id])
          delete uporabnikiNaKanalih[trenutniKanal[socket.id]][i];
          break;
        }
      }
    }
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
    
    posodobiUporabnikeNaKanalih(trenutniKanal[socket.id],socket);
  });
}